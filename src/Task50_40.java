import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class Task50_40 {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        // task 1
        System.out.println("\nTask 1:");
        int x1 = 4, y1 = 7;
        int x2 = -4;
        System.out.println("Array mới được tạo khi x=4, y=7 là: " + Arrays.toString(task1(x1, y1)));
        System.out.println("Array mới được tạo khi x=-4, y=7 là: " + Arrays.toString(task1(x2, y1)));

        // task 2
        System.out.println("\nTask 2:");
        int[] arr2 = { 1, 2, 3 }, arr2_1 = { 100, 2, 1, 10 };
        int[] arr2_2 = { 1, 2, 3 }, arr2_3 = { 1, 2, 3 };
        System.out.println("arr2 + arr2_1 = " + Arrays.toString(task2(arr2, arr2_1)));
        System.out.println("arr2_2 + arr2_3 = " + Arrays.toString(task2(arr2_2, arr2_3)));

        // task 3
        System.out.println("\nTask 3:");
        int[] arr3 = { 1, 2, 1, 4, 5, 1, 1, 3, 1 };
        System.out.println("n=1: " + task3(arr3, 1));
        System.out.println("n=2: " + task3(arr3, 2));

        // task 4
        System.out.println("\nTask 4:");
        int[] arr4 = { 1, 2, 3, 4, 5, 6 };
        System.out.println("Tổng các phần tử trong mảng arr4 là: " + task4(arr4));

        // task 5
        System.out.println("\nTask 5:");
        int[] arr5 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        for (int i = 0; i < task5(arr5).length; i++) {
            if (task5(arr5)[i] != 0) {
                System.out.print(task5(arr5)[i] + " ");
            }
        }

        // task 6
        System.out.println("\nTask 6:");
        int[] arr6_1 = { 1, 0, 2, 3, 4 };
        int[] arr6_2 = { 3, 5, 6, 7, 8, 13 };
        for (int i = 0; i < task6(arr6_1, arr6_2).length; i++) {
            System.out.print(task6(arr6_1, arr6_2)[i] + " ");
        }

        // task 7
        System.out.println("\nTask 7:");
        int[] arr7 = { 1, 2, 3, 1, 5, 1, 4, 6, 3, 4 };
        for (int i = 0; i < task7(arr7).length; i++) {
            System.out.print(task7(arr7)[i] + " ");
        }

        // task 8
        System.out.println("\nTask 8:");
        int[] arr8_1 = { 1, 2, 3 };
        int[] arr8_2 = { 100, 2, 1, 10 };
        for (int i = 0; i < task8(arr8_1, arr8_2).length; i++) {
            System.out.print(task8(arr8_1, arr8_2)[i] + " ");
        }

        // task 9
        System.out.println("\nTask 9:");
        int[] arr9 = { 1, 3, 1, 4, 2, 5, 6 };
        task9(arr9);
        for (int i = 0; i < arr9.length; i++) {
            System.out.print(arr9[i] + " ");
        }

        // task 10
        System.out.println("\nTask 10:");
        int[] arr10_1 = { 10, 20, 30, 40, 50 };
        int x = 0;
        int y = 2;
        System.out.println("Before swapping:");
        System.out.println(Arrays.toString(arr10_1));

        swap(arr10_1, x, y);

        System.out.println("After swapping:");
        System.out.println(Arrays.toString(arr10_1));
    }

    // method task 1: Tạo một mảng gồm các phần tử có giá trị từ x đến y
    public static int[] task1(int x, int y) {
        int arr[] = new int[y - x + 1];
        for (int i = 0; i < (y - x + 1); i++) {
            // fill array with values starting from x to y
            arr[i] = i + x;
        }
        return arr;
    }

    // method task 2: Gộp 2 mảng lại với nhau, bỏ hết các phần tử có giá trị trùng
    // nhau
    public static int[] task2(int[] arr1, int[] arr2) {
        int[] merged = new int[arr1.length + arr2.length];
        System.arraycopy(arr1, 0, merged, 0, arr1.length);
        System.arraycopy(arr2, 0, merged, arr1.length, arr2.length);

        Set<Integer> nodupes = new HashSet<Integer>();

        for (int i = 0; i < merged.length; i++) {
            nodupes.add(merged[i]);
        }

        int[] nodupesarray = new int[nodupes.size()];
        int i = 0;
        Iterator<Integer> it = nodupes.iterator();
        while (it.hasNext()) {
            nodupesarray[i] = it.next();
            i++;
        }
        return nodupesarray;
    }

    // method task 3: Đếm số lần xuất hiện của phần tử n trong mảng
    public static int task3(int[] arr, int n) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == n) {
                count++;
            }
        }
        return count;
    }

    // method task 4: Tính tổng các phần tử trong mảng
    public static int task4(int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }
        return sum;
    }

    // method task 5: Từ một mảng cho trước, tạo một mảng gồm toàn các số chẵn
    public static int[] task5(int[] arr) {
        int[] evenArr = new int[arr.length];
        int j = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) {
                evenArr[j] = arr[i];
                j++;
            }
        }
        return evenArr;
    }

    // method task 6: Từ 2 mảng cho trước,
    // tạo một mảng mới trong đó mỗi phần tử là tổng của 2 phần tử ở vị trí tương
    // ứng của 2 mảng đã cho
    public static int[] task6(int[] arr1, int[] arr2) {
        int[] sumArr = new int[Math.min(arr1.length, arr2.length)];
        for (int i = 0; i < sumArr.length; i++) {
            sumArr[i] = arr1[i] + arr2[i];
        }
        return sumArr;
    }

    // method task 7: Từ 1 mảng cho trước tạo một mảng mới gồm các phần tử của mảng
    // cũ nhưng không bao gồm các phần tử có giá trị trùng nhau
    public static int[] task7(int[] arr) {
        int[] distinctArr = new int[arr.length];
        int distinctIndex = 0;
        boolean isDistinct = true;
        for (int i = 0; i < arr.length; i++) {
            isDistinct = true;
            for (int j = 0; j < i; j++) {
                if (arr[i] == arr[j]) {
                    isDistinct = false;
                    break;
                }
            }
            if (isDistinct) {
                distinctArr[distinctIndex] = arr[i];
                distinctIndex++;
            }
        }
        int[] finalDistinctArr = new int[distinctIndex];
        for (int i = 0; i < distinctIndex; i++) {
            finalDistinctArr[i] = distinctArr[i];
        }
        return finalDistinctArr;
    }

    // method task 8: Lấy những phần tử có giá trị không trùng nhau của 2 mảng cho
    // trước
    public static int[] task8(int[] arr1, int[] arr2) {
        Set<Integer> set1 = new HashSet<>();
        Set<Integer> set2 = new HashSet<>();
        for (int i = 0; i < arr1.length; i++) {
            set1.add(arr1[i]);
        }
        for (int i = 0; i < arr2.length; i++) {
            set2.add(arr2[i]);
        }
        set1.retainAll(set2); // giữ lại các phần tử giống nhau
        Set<Integer> distinctSet = new HashSet<>();
        for (int i = 0; i < arr1.length; i++) {
            if (!set1.contains(arr1[i])) {
                distinctSet.add(arr1[i]);
            }
        }
        for (int i = 0; i < arr2.length; i++) {
            if (!set1.contains(arr2[i])) {
                distinctSet.add(arr2[i]);
            }
        }
        int[] distinctArr = new int[distinctSet.size()];
        int i = 0;
        for (int value : distinctSet) {
            distinctArr[i] = value;
            i++;
        }
        return distinctArr;
    }

    // method task 9: Sắp xếp các phần tử theo giá trị giảm dần
    public static void task9(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j] < arr[j + 1]) {
                    // swap arr[j] and arr[j+1]
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
    }

    // method task 10: Đổi phần tử từ vị trí thứ x sang vị trí thứ y
    public static void swap(int[] arr, int x, int y) {
        int temp = arr[x];
        arr[x] = arr[y];
        arr[y] = temp;
    }

}