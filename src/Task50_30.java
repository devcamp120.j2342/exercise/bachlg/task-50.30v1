import java.util.Arrays;

public class Task50_30 {
    public static void main(String[] args) {
        System.out.println("Is the input a string? " + isString("Devcamp"));
        System.out.println("Is the input a string? " + isString(1));

        String input1 = "Robin Singh";
        int n = 4;
        String substring = getSubstring(input1, n);
        System.out.println("Substring: " + substring);

        String input2 = "Robin Singh";
        String[] wordsArray = splitString(input2);
        System.out.println("Array of words: " + Arrays.toString(wordsArray));

        String input3 = "Robin Singh from USA";
        String formattedString = formatString(input3);
        System.out.println("Formatted string: " + formattedString);

        String input4 = "JavaScript Exercises";
        String trimmedString = removeSpaces(input4);
        System.out.println("Trimmed string: " + trimmedString);

        String input5 = "JavaScript Exercises";
        String capitalizedString = capitalizeWords(input5);
        System.out.println("Capitalized string: " + capitalizedString);

        String input6 = "js string exercises";
        String titleCaseString = titleCase(input6);
        System.out.println("Title case string: " + titleCaseString);

        String input7 = "Ha!";
        int repetitions = 3;
        String repeatedString = repeatString(input7, repetitions);
        System.out.println("Repeated string: " + repeatedString);

        String input8 = "dcresource";
        int chunkSize = 2;
        String[] chunkedArray = chunkString(input8, chunkSize);
        System.out.println("Chunked array: " + Arrays.toString(chunkedArray));

        String input9 = "the quick brown fox jumps over the lazy dog";
        String subString = "the";
        int count = countSubstringOccurrences(input9, subString);
        System.out.println("Substring count: " + count);

        String input10 = "000";
        String replacement = "123";
        int replaceCount = 2;
        String replacedString = replaceRight(input10, replacement, replaceCount);
        System.out.println("Replaced string: " + replacedString);
    }

    public static boolean isString(Object input) {
        return input instanceof String;
    }

    public static String getSubstring(String input, int n) {
        if (input == null || n < 0 || n >= input.length()) {
            return input;
        }
        return input.substring(0, n);
    }

    public static String[] splitString(String input) {
        if (input == null) {
            return new String[0];
        }
        return input.split(" ");
    }

    public static String formatString(String input) {
        if (input == null) {
            return input;
        }
        return input.toLowerCase().replace(" ", "-");
    }

    public static String removeSpaces(String input) {
        if (input == null) {
            return input;
        }
        return input.replace(" ", "");
    }

    public static String capitalizeWords(String input) {
        if (input == null) {
            return input;
        }
        String[] words = input.split(" ");
        StringBuilder sb = new StringBuilder();
        for (String word : words) {
            if (word.length() > 0) {
                char firstChar = Character.toUpperCase(word.charAt(0));
                sb.append(firstChar).append(word.substring(1)).append(" ");
            }
        }
        return sb.toString().trim();
    }

    public static String titleCase(String input) {
        if (input == null || input.isEmpty()) {
            return input;
        }

        String[] words = input.toLowerCase().split(" ");
        StringBuilder result = new StringBuilder();

        for (String word : words) {
            if (!word.isEmpty()) {
                char firstChar = Character.toUpperCase(word.charAt(0));
                String remainingChars = word.substring(1);
                result.append(firstChar).append(remainingChars).append(" ");
            }
        }

        return result.toString().trim();
    }

    public static String repeatString(String input, int repetitions) {
        if (input == null || input.isEmpty() || repetitions <= 0) {
            return "";
        }

        StringBuilder result = new StringBuilder();
        for (int i = 0; i < repetitions; i++) {
            result.append(input).append(" ");
        }

        return result.toString().trim();
    }

    public static String[] chunkString(String input, int chunkSize) {
        if (input == null || input.isEmpty() || chunkSize <= 0) {
            return new String[0];
        }

        int length = input.length();
        int chunkCount = (int) Math.ceil((double) length / chunkSize);
        String[] result = new String[chunkCount];

        for (int i = 0; i < chunkCount; i++) {
            int startIndex = i * chunkSize;
            int endIndex = Math.min(startIndex + chunkSize, length);
            result[i] = input.substring(startIndex, endIndex);
        }

        return result;
    }

    public static int countSubstringOccurrences(String input, String subString) {
        if (input == null || input.isEmpty() || subString == null || subString.isEmpty()) {
            return 0;
        }

        int count = 0;
        int index = input.indexOf(subString);
        while (index != -1) {
            count++;
            index = input.indexOf(subString, index + 1);
        }

        return count;
    }

    public static String replaceRight(String input, String replacement, int replaceCount) {
        if (input == null || input.isEmpty() || replacement == null || replacement.isEmpty() || replaceCount <= 0) {
            return input;
        }

        int length = input.length();
        int startIndex = Math.max(length - replaceCount, 0);

        StringBuilder result = new StringBuilder(input);
        result.replace(startIndex, length, replacement);

        return result.toString();
    }
}
