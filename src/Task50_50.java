public class Task50_50 {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
    // task 1:
        System.out.println("\nTask 1:");
        String str1 = "DCresource: JavaScript Exercises";
        System.out.println(task1(str1, 'e'));

        // task 2:
        System.out.println("\nTask 2:");
        String str2_1 = "dcresource ";
        String str2_2 = " dcresource";
        String str2_3 = " dcresource ";
        System.out.println(task2(str2_1));
        System.out.println(task2(str2_2));
        System.out.println(task2(str2_3));

        // task 3:
        System.out.println("\nTask 3:");
        String str3 = "The quick brown fox jumps over the lazy dog";
        System.out.println(task3(str3, "the"));

        // task 4:
        System.out.println("\nTask 4:");
        String str4 = "JS PHP PYTHON";
        System.out.println(task4(str4, "PYTHON"));
        System.out.println(task4(str4, "JS"));

        // task 5:
        System.out.println("\nTask 5:");
        String str5_1 = "abcd";
        String str5_2 = "AbcD";
        String str5_3 = "ABCD";
        String str5_4 = "Abce";
        System.out.println(task5(str5_1, str5_2));
        System.out.println(task5(str5_3, str5_4));

        // task 6:
        System.out.println("\nTask 6:");
        String str6 = "Js STRING EXERCISES";
        System.out.println(task6(str6, 1));
        System.out.println(task6(str6, 2));

        // task 7:
        System.out.println("\nTask 7:");
        String str7 = "Js STRING EXERCISES";
        System.out.println(task7(str7, 1));
        System.out.println(task7(str7, 2));

        // task 8:
        System.out.println("\nTask 8:");
        String str8 = "js string exercises";
        System.out.println(task8(str8, "js"));

        // task 9:
        System.out.println("\nTask 9:");
        String str9_1 = "abc";
        String str9_2 = "";
        System.out.println(task9(str9_1));
        System.out.println(task9(str9_2));

        // task 10:
        System.out.println("\nTask 10:");
        String str10 = "AaBbc";
        System.out.println(task10(str10));
    }

    // task 1: Đếm số lần xuất hiện của ký tự n trong chuỗi
    public static int task1(String str, char n) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == n) {
                count++;
            }
        }
        return count;
    }

    // task 2: Loại bỏ các ký tự trắng ở đầu và cuối chuỗi
    public static String task2(String str) {
        int start = 0;
        int end = str.length() - 1;
        while (start <= end && Character.isWhitespace(str.charAt(start))) {
            start++;
        }
        while (end >= start && Character.isWhitespace(str.charAt(end))) {
            end--;
        }
        return str.substring(start, end + 1);
    }

    // task 3: Loại bỏ chính xác chuỗi con ra khỏi chuỗi cho trước
    public static String task3(String str, String sub) {
        int index = str.indexOf(sub);
        if (index != -1) {
            StringBuilder builder = new StringBuilder(str);
            builder.replace(index, index + sub.length(), "");
            return builder.toString();
        }
        return str;
    }

    // task 4: Kiểm tra chuỗi sau có phải là kết thúc của chuỗi trước hay không
    public static boolean task4(String str, String endStr) {
        if (str.endsWith(endStr)) {
            return true;
        } else {
            return false;
        }
    }

    // task 5: So sánh 2 chuỗi có giống nhau hay không
    public static boolean task5(String str1, String str2) {
        // Chuyển đổi hai chuỗi về cùng một kiểu chữ cái
        str1 = str1.toLowerCase();
        str2 = str2.toLowerCase();
        // So sánh hai chuỗi
        return str1.equals(str2);
    }

    // task 6: Kiểm tra ký tự thứ n của chuỗi có phải viết hoa hay không
    public static boolean task6(String str, int n) {
        if (n > str.length()) {
            System.out.println("Invalid position number!");
            return false;
        }
        char ch = str.charAt(n-1);
        return Character.isUpperCase(ch);
    }

    // task 7: Kiểm tra ký tự thứ n của chuỗi có phải viết thường hay không
    public static boolean task7(String str, int n) {
        if (n > str.length()) {
            System.out.println("Invalid position number!");
            return false;
        }
        char ch = str.charAt(n-1);
        return Character.isLowerCase(ch);
    }

    // task 8: Kiểm tra chuỗi trước có bắt đầu bằng chuỗi sau hay không
    public static boolean task8(String str1, String str2) {
        if (str1.startsWith(str2)) {
            return true;
        } else {
            return false;
        }
    }

    // task 9: Kiểm tra chuỗi đã cho có phải chuỗi rỗng hay không
    public static boolean task9(String str) {
        return str == null || str.trim().isEmpty();
    }

    // task 10: Đảo ngược chuỗi
    public static String task10(String str) {
        StringBuilder sb = new StringBuilder(str);
        sb.reverse();
        return sb.toString();
    }
}